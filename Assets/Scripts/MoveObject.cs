﻿using UnityEngine;

[DisallowMultipleComponent] //запретить множественные компоненты
public class MoveObject : MonoBehaviour
{
    [SerializeField] Vector3 movePosition;
    [SerializeField] [Range(0,1)] float moveProgress; // Range(0,1) - ползунок. 0 - не двигался, 0,5 - 50%, 1 - полностью сдвинулся
    [SerializeField] float moveSpeed; // скорость движения
    Vector3 startPosition;

    void Start()
    {
        startPosition = transform.position; //стартовая позиция
    }

    // Update is called once per frame
    void Update()
    {
        moveProgress = Mathf.PingPong(Time.time * moveSpeed, 1); //позволяет двигать значения. Используем Time.time, как переменную, которая постоянно растет
        Vector3 offset = movePosition * moveProgress; // offset - сдвиг
        transform.position = startPosition + offset;
    }
}
