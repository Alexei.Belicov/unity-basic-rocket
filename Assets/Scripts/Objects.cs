﻿using UnityEngine;

public class Objects : MonoBehaviour
{
    Rigidbody rigidBody;
    AudioSource audioSource;
    [SerializeField] ParticleSystem StartPlatformParticles;

    //State state = State.Playing;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionObjectsEnter(Collision collision)
    {
        // if (state == State.Dead || state == State.NextLevel)    
        // {
        //     return; // прекращает выполнение остальной части функции
        // }

        switch (collision.gameObject.tag) //при соприкосновении с объектом - получаем доступ к его тегу
        {
            case "Friendly":
                print("Активированн Object");
                StartPlatform();
                break;
        }

        void StartPlatform()
        {
            StartPlatformParticles.Play();
        }
    }
}
