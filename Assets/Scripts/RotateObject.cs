﻿using UnityEngine;

[DisallowMultipleComponent] //запретить множественные компоненты

public class RotateObject : MonoBehaviour
{
    [SerializeField] float rotateSpeedX = 50f;
    [SerializeField] float rotateSpeedY = 50f;
    [SerializeField] float rotateSpeedZ = 50f;
    [SerializeField] float rotateLengthX = 90f;
    [SerializeField] float rotateLengthY = 90f;
    [SerializeField] float rotateLengthZ = 90f;

    void Update()
    {
        RotationMixed();
    }

    void RotationMixed()
    {
        transform.rotation = Quaternion.Euler(
            Mathf.PingPong(Time.time * rotateSpeedX, rotateLengthX), 
            Mathf.PingPong(Time.time * rotateSpeedY, rotateLengthY), 
            Mathf.PingPong(Time.time * rotateSpeedZ, rotateLengthZ));
    }
}