﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI; // библиотека нужна для переключения уровней

public class Rocket : MonoBehaviour
{
    Rigidbody rigidBody;
    AudioSource audioSource;

    [SerializeField] float flySpeed = 50f; // скорость полета при удержании Space
    [SerializeField] float rotationSpeed = 50f; // скорость поворота на клавиши A и D
    [SerializeField] int energyTotal = 500; // энергия ракеты
    [SerializeField] int energyApply = 50; // энергия ракеты
    [SerializeField] int energyBattery; // кол-во энергии батарейки

    [SerializeField] Text energyText; // энергия ракеты

    [SerializeField] AudioClip flySound; // звук полета при удержании Space
    [SerializeField] AudioClip deathSound; // звук при взрыве
    [SerializeField] AudioClip fakePlatformDeathSound; // звук при взрыве
    [SerializeField] AudioClip fakePlatformSound; // звук при взрыве
    [SerializeField] AudioClip finishSound; // звук победы
    [SerializeField] AudioClip batterySound; // звук батарейки

    [SerializeField] ParticleSystem flyParticles; // эффект полета
    [SerializeField] ParticleSystem deathParticles; // эффект взрыва
    [SerializeField] ParticleSystem fakePlatformDeathParticles; // эффект взрыва
    [SerializeField] ParticleSystem finishParticles; // эффект победы

    bool collisionOff = false; // включенные коллизии

    enum State //состояния игры
    {
        Playing,
        Dead,
        NextLevel
    }
    State state = State.Playing;

    void Start()
    {
        energyText.text = energyTotal.ToString(); // отображение кол-ва энергии при старте 
        rigidBody = GetComponent<Rigidbody>(); // получаем возможность использовать Rigidbody именно этой ракеты
        audioSource = GetComponent<AudioSource>(); // получаем возможность использовать AudioSource именно этой ракеты
        state = State.Playing;
    }

    void Update()
    {
        if (state == State.Playing) //если статус не Playing, то забираем управление
        {
            Launch();
            Rotation();
        }

        if (Debug.isDebugBuild) // Чит-коды будут компайлиться, только в Debug сборке. isDebugBuild - false по умолчанию
        {
            DebugKeys();
        }
    }

    void Launch()
    {
        if (Input.GetKey(KeyCode.Space) && energyTotal > 0) //если зажали Space и энергия больше 5, то выполняется код
        {
            energyTotal -= Mathf.RoundToInt(energyApply * Time.deltaTime); // отнимаем энергию
            energyText.text = energyTotal.ToString(); // пишем в UI кол-во энергии. Конвертируем Int в String

            rigidBody.AddRelativeForce(Vector3.up * (flySpeed * Time.deltaTime));
            if (audioSource.isPlaying == false) //если зажат Space и звук не играет, то включить звук
            {
                audioSource.PlayOneShot(flySound);
                flyParticles.Play(); // запустить эффект полета
            }
            //audioSource.Play(); // один звук через Audio Source
        }
        else
        {
            audioSource.Pause(); // приостановить звук
            flyParticles.Stop(); // приостановить эффект полета
        }
    }

    void Rotation()
    {
        float rotationSpeedFixed = rotationSpeed * Time.deltaTime;

        rigidBody.freezeRotation = true; //заморозить вращение
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward * rotationSpeedFixed); //new Vector3(0, 0, 1); = Vector3.forward
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(-Vector3.forward * rotationSpeedFixed); //new Vector3(0, 0, -1);
        }
        rigidBody.freezeRotation = false; //разморозить вращение
    }

    void OnCollisionEnter(Collision collision) // запускается при соприкосновении колллайдеров
    {
        if (state == State.Dead || state == State.NextLevel || collisionOff) // либо умерли, либо загружаем следующий уровень, либо загружаем чит-коды
        {
            return; // прекращает выполнение остальной части функции
        }

        switch (collision.gameObject.tag) //при соприкосновении с объектом - получаем доступ к его тегу
        {
            case "Battery":
                print("Коснулись батарейки");
                PlusEnergy(energyBattery, collision.gameObject); // принимает два параметра - энергию, которую хотим добавить и объект батарейки, до которой мы дотронулись
                break;
            case "Finish":
                Finish();
                break;
            case "Friendly":
                //print("Коснулись дружественного объекта");
                break;
            case "FakePlatform":
                FakePlatform();
                break;
            default:
                Death();
                break;
        }
    }

    private void Finish()
    {
        state = State.NextLevel;
        audioSource.Stop(); // остановить звук
        audioSource.PlayOneShot(finishSound);
        finishParticles.Play();
        Invoke("LoadNextLevel", 2f); // пишем только название метода и время задержки
    }

    private void Death()
    {
        state = State.Dead;
        audioSource.Stop(); // остановить звук
        audioSource.PlayOneShot(deathSound);
        deathParticles.Play();
        Invoke("LoadFirstLevel", 3.5f); // пишем только название метода и время задержки
    }

    private void FakePlatform()
    {
        state = State.Dead;
        audioSource.Stop(); // остановить звук
        audioSource.PlayOneShot(fakePlatformDeathSound);
        audioSource.PlayOneShot(fakePlatformSound);
        fakePlatformDeathParticles.Play(); //вешаю на платформу, т.к. на ракете эффекты крутится вместе с ракетой
    }

    private void PlusEnergy(int energyToAdd, GameObject batteryObject)
    {
        audioSource.Stop();
        audioSource.PlayOneShot(batterySound);
        batteryObject.GetComponent<BoxCollider>().enabled = false; // Получаем доступ к коллайдеру батарейки. Отключем коллайдер, что засчиталось только одно касание
        energyTotal += energyToAdd; // добавляем энергию
        energyText.text = energyTotal.ToString(); // сразу обновляем значение энергии, без нажатия на пробел
        Destroy(batteryObject);
    }

    private void LoadFirstLevel() // проигрыш - возврат на 1-й уровень
    {
        SceneManager.LoadScene(0); //загрузить первый уровень
    }

    private void LoadNextLevel() // выигрыш - переход на следующий уровень
    {
        int currentLevelIndex = SceneManager.GetActiveScene().buildIndex; // сцена в которой находимся - GetActiveScene. индекс этой сцены - buildIndex
        int nextLevelIndex = currentLevelIndex + 1;

        if (nextLevelIndex == SceneManager.sceneCountInBuildSettings) // sceneCountInBuildSettings - кол-во сцен в build settings
        {
            nextLevelIndex = 0; // возврат к 1-му уровню, если прошли последний уровень
        }

        SceneManager.LoadScene(nextLevelIndex);
    }

    private void DebugKeys() // Чит-коды. Проверка нажатия клавиш
    {
        // Переход на следующий уровень
        if (Input.GetKeyDown(KeyCode.L)) // GetKeyDown - проверяем, если нажали кнопку L
        {
            LoadNextLevel();
        }
        // Бессмертие
        else if (Input.GetKeyDown(KeyCode.C)) // проверяем, если нажали кнопку C
        {
            collisionOff = !collisionOff; // если слева false, то меняем на true и наоборот
        }
    }
}